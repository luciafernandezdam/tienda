<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property int|null $precio
 * @property string|null $stock
 * @property int|null $id_proveedor
 * @property int|null $id_categoria
 * @property int|null $id_empleado
 *
 * @property Detalles[] $detalles
 * @property Ventas[] $ventas
 * @property Categorias $categoria
 * @property Empleados $empleado
 * @property Proveedores $proveedor
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'precio', 'id_proveedor', 'id_categoria', 'id_empleado'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
            [['descripcion'], 'string', 'max' => 50],
            [['stock'], 'string', 'max' => 10],
            [['id_proveedor', 'id_categoria', 'id_empleado'], 'unique', 'targetAttribute' => ['id_proveedor', 'id_categoria', 'id_empleado']],
            [['id'], 'unique'],
            [['id_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::className(), 'targetAttribute' => ['id_categoria' => 'id']],
            [['id_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['id_empleado' => 'id']],
            [['id_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['id_proveedor' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Marca',
            'descripcion' => 'Descripcion',
            'precio' => 'Precio',
            'stock' => 'Stock',
            'id_proveedor' => 'Id Prov',
            'id_categoria' => 'Id Cate',
            'id_empleado' => 'Id Emp',
        ];
    }

    /**
     * Gets query for [[Detalles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDetalles()
    {
        return $this->hasMany(Detalles::className(), ['id_producto' => 'id']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::className(), ['id' => 'id_venta'])->viaTable('detalles', ['id_producto' => 'id']);
    }

    /**
     * Gets query for [[Categoria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria()
    {
        return $this->hasOne(Categorias::className(), ['id' => 'id_categoria']);
    }

    /**
     * Gets query for [[Empleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleado()
    {
        return $this->hasOne(Empleados::className(), ['id' => 'id_empleado']);
    }

    /**
     * Gets query for [[Proveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProveedor()
    {
        return $this->hasOne(Proveedores::className(), ['id' => 'id_proveedor']);
    }
}
