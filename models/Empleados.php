<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empleados".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $edad
 * @property string|null $direccion
 * @property int|null $nº_ventas
 * @property int|null $horas_trabajadas
 * @property int|null $salario
 *
 * @property Productos[] $productos
 * @property Ventas[] $ventas
 * @property Clientes[] $clientes
 */
class Empleados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'num_ventas', 'horas_trabajadas', 'salario'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
            [['apellidos'], 'string', 'max' => 30],
            [['edad'], 'string', 'max' => 5],
            [['direccion'], 'string', 'max' => 50],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'edad' => 'Edad',
            'direccion' => 'Direccion',
            'num_ventas' => 'Numero Ventas',
            'horas_trabajadas' => 'Horas Trabajadas',
            'salario' => 'Salario',
        ];
    }

    /**
     * Gets query for [[Productos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Productos::className(), ['id_empleado' => 'id']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::className(), ['id_empleado' => 'id']);
    }

    /**
     * Gets query for [[Clientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClientes()
    {
        return $this->hasMany(Clientes::className(), ['id' => 'id_cliente'])->viaTable('ventas', ['id_empleado' => 'id']);
    }
}
