<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ventas".
 *
 * @property int $id
 * @property string|null $fecha_venta
 * @property int|null $descuento
 * @property int|null $recaudacion_final
 * @property int|null $id_empleado
 * @property int|null $id_cliente
 *
 * @property Detalles[] $detalles
 * @property Productos[] $productos
 * @property Clientes $cliente
 * @property Empleados $empleado
 */
class Ventas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_venta'], 'safe'],
            [['descuento', 'recaudacion_final', 'id_empleado', 'id_cliente'], 'integer'],
            [['id_empleado', 'id_cliente'], 'unique', 'targetAttribute' => ['id_empleado', 'id_cliente']],
            [['id_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['id_cliente' => 'id']],
            [['id_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['id_empleado' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha_venta' => 'Fecha Venta',
            'descuento' => 'Descuento',
            'recaudacion_final' => 'Recaudacion Final',
            'id_empleado' => 'Id Emp',
            'id_cliente' => 'Id Clie',
        ];
    }

    /**
     * Gets query for [[Detalles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDetalles()
    {
        return $this->hasMany(Detalles::className(), ['id_venta' => 'id']);
    }

    /**
     * Gets query for [[Productos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Productos::className(), ['id' => 'id_producto'])->viaTable('detalles', ['id_venta' => 'id']);
    }

    /**
     * Gets query for [[Cliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Clientes::className(), ['id' => 'id_cliente']);
    }

    /**
     * Gets query for [[Empleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleado()
    {
        return $this->hasOne(Empleados::className(), ['id' => 'id_empleado']);
    }
}
