<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $telefono
 * @property string|null $correo_electronico
 * @property string|null $direccion
 * @property string|null $forma_pago
 * @property string|null $web
 *
 * @property Productos[] $productos
 */
class Proveedores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['nombre', 'correo_electronico', 'forma_pago', 'web'], 'string', 'max' => 20],
            [['telefono'], 'string', 'max' => 10],
            [['direccion'], 'string', 'max' => 50],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'telefono' => 'Telefono',
            'correo_electronico' => 'Correo Electronico',
            'direccion' => 'Direccion',
            'forma_pago' => 'Forma Pago',
            'web' => 'Web',
        ];
    }

    /**
     * Gets query for [[Productos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Productos::className(), ['id_proveedor' => 'id']);
    }
}
