<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detalles';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="detalles-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            //'id_producto',
            [
                'attribute'=>'Producto',
                'value'=>'producto.nombre'
            ],
            //'id_venta',
            [
                'attribute'=>'Venta',
                'value'=>'venta.fecha_venta'
            ],
            'cantidad',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
    <p style="text-align: right">
        <?= Html::a('Crear detalle', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <br>

</div>
