<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Empleados';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="empleados-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            'nombre',
            'apellidos',
            'edad',
            'direccion',
            'num_ventas',
            'horas_trabajadas',
            'salario',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
    <p style="text-align: right">
        <?= Html::a('Crear empleado', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <br>
</div>
