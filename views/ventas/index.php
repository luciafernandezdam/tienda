<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ventas';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ventas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
            'id',
            [
                'attribute'=>'Empleado',
                'value'=>'empleado.nombre'
            ],
            [
                'attribute'=>'Cliente',
                'value'=>'cliente.nombre'
            ],
            'fecha_venta',
            'descuento',
            'recaudacion_final',
            //'id_empleado',
            //'id_cliente',
            

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
    <p style="text-align: right">
        <?= Html::a('Crear venta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


</div>
