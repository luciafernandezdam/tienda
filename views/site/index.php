<?php

use yii\helpers\Html;
$conexion=mysqli_connect('localhost','root','','tienda');
/* @var $this yii\web\View */

$this->title = 'Handball-Luc-Sport';
if (Yii::$app->user->isGuest){
?>

<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>  
  
  <style>
  
  body {
    font: 13px;
    line-height: 1.8;
    color: #f5f6f7;
    background-color:#000B31; 
    max-widht:1200px; 
    margin: auto; 
    padding: 0px;
  }
  p {font-size: 14px;}
  .margin {margin-bottom: 35px;}
  .bg-1 { 
    background-color: skyblue; 
    color: black;
    font-family: verdana;
  }
  .bg-2 { 
    background-color: #474e5d; 
    color: #ffffff;
    font-family: verdana;
  }
  .bg-3 { 
    background-color: #7390E1; 
    color: black;
    font-family: verdana;
  }
  .bg-4 { 
    background-color: sandybrown; 
    color: black;
    font-family: verdana;
    align-items: center;
  }
  .container-fluid {
    padding-top: 50px;
    padding-bottom: 50px;
    margin-right: auto;
    margin-left: auto;
    font-family: verdana;
  }
  .table{
      background-color: white;
      border-collapse: collapse;
  }
  
  </style>
</head>

<body>

<div class="site-index">
<!-- Primer contenedor -->
<div class="container-fluid bg-1 text-center">
    <h1 class="margin"><b>Handball-Luc-Sport</b></h1>
    <img src="https://gitlab.com/luciafernandezdam/tienda/-/raw/master/logo/Logo.png" class="img-responsive img-circle margin" style="display:inline" alt="logo" width="250" height="250">
  <h3>Tienda de deportes especializado en Balonmano</h3>
</div>

<!-- Segundo contenedor -->
<div class="container-fluid bg-2 text-center">
  <h3 class="lead">Bienvenido a nuestra aplicacion para la administracion de la tienda!</h3>
  <p> Aplicacion que lleva toda la gestion de los datos de la tienda sin ninguna perdida. Para ello es necesario iniciar sesion mediante el boton de abajo. </p>
  <br>
  <p><?= Html::a('Iniciar Sesion &raquo;', ['site/login'], ['class' => 'btn btn-lg btn-success']) ?></p>

</div>

<!-- Tercer contenedor (Grid) -->
<div class="container-fluid bg-3 text-center">    
    <h3 style="text-align: center; text-decoration: underline" ><b>Acceso directo a todas las plantillas</b></h3>
  <br>
  <div class="row" style="font-family: verdana">
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <img src="https://static.vecteezy.com/system/resources/thumbnails/000/593/712/small/40_281.jpg" class="img-circle" alt="">
              <div class="caption">
                  <h3><b>Productos</b></h3>
                <p>Consulta de todos los productos que han sido recibidos.</p>
                <br>
                <p><?= Html::a('Acceder a Productos &raquo;', ['productos/index'], ['class' => 'btn btn-default']) ?></p>
              </div>
            </div>
          </div>
      
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <img src="https://static.vecteezy.com/system/resources/thumbnails/000/550/731/small/user_icon_004.jpg" class="img-circle" alt="">
              <div class="caption">
                  <h3><b>Clientes</b></h3>
                <p>Consulta todos los datos de nuestros clientes que hayan comprado en la tienda.</p>
                <br>
                <p><?= Html::a('Acceder a Clientes &raquo;', ['clientes/index'], ['class' => 'btn btn-default']) ?></p>
              </div>
            </div>
          </div>
      
         <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
              <img src="https://static.vecteezy.com/system/resources/thumbnails/000/548/349/small/264_-_1_-_Discount_Label.jpg" class="img-circle" alt="512x512">
              <div class="caption">
                  <h3><b>Ventas</b></h3>
                <p>Consulta todas las ventas que se hayan realizado en la tienda.</p>
                <br>
                <p><?= Html::a('Acceder a Ventas &raquo;', ['ventas/index'], ['class' => 'btn btn-default']) ?></p>
              </div>
            </div>
          </div>
         
            <br>
        </div>

    </div>

    <div class="container-fluid bg-4 text-center">
        <h3 style="text-align: center; font-style: bold" >
            <svg class="bi bi-award" width="2em" height="1em" viewBox="0 0 20 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M9.669.864L8 0 6.331.864l-1.858.282-.842 1.68-1.337 1.32L2.6 6l-.306 1.854 1.337 1.32.842 1.68 1.858.282L8 12l1.669-.864 1.858-.282.842-1.68 1.337-1.32L13.4 6l.306-1.854-1.337-1.32-.842-1.68L9.669.864zm1.196 1.193l-1.51-.229L8 1.126l-1.355.702-1.51.229-.684 1.365-1.086 1.072L3.614 6l-.25 1.506 1.087 1.072.684 1.365 1.51.229L8 10.874l1.356-.702 1.509-.229.684-1.365 1.086-1.072L12.387 6l.248-1.506-1.086-1.072-.684-1.365z"/>
            <path d="M4 11.794V16l4-1 4 1v-4.206l-2.018.306L8 13.126 6.018 12.1 4 11.794z"/>
            </svg>
            <b>Top 10 de los mejores empleados en ventas</b></h3><br>
        <center>
        <table class="table table-striped" border="4" style="text-align: center; width: 85%; background-color: white" >
		<tr>
                    <td><b>Id</b></td>
                    <td><b>Nombre</b></td>
                    <td><b>Apellidos</b></td>
                    <td><b>Edad</b></td>
                    <td><b>Numero Ventas</b></td>
		</tr>

		<?php 
		$sql="SELECT id,nombre,apellidos,edad,num_ventas from empleados ORDER BY num_ventas DESC LIMIT 10";
		$result=mysqli_query($conexion,$sql);

		while($mostrar=mysqli_fetch_array($result)){
		 ?>

		<tr>
			<td><?php echo $mostrar['id'] ?></td>
			<td><?php echo $mostrar['nombre'] ?></td>
			<td><?php echo $mostrar['apellidos'] ?></td>
			<td><?php echo $mostrar['edad'] ?></td>
                        <td><?php echo $mostrar['num_ventas'] ?></td>
		</tr>
	<?php 
	}
	 ?>
	</table>
        </center>
    </div>
</div>
</body>
</html>

<?php } ?>
<?php

$conexion=mysqli_connect('localhost','root','','tienda');
/* @var $this yii\web\View */

$this->title = 'Handball-Luc-Sport';
if (!Yii::$app->user->isGuest){
?>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">-->
<!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>  -->
  
  <style>
  
  body {
    font: 13px;
    line-height: 1.8;
    color: #f5f6f7;
    background-color:#000B31; 
    max-widht:1200px; 
    margin: auto; 
    padding: 0px;
  }
  p {font-size: 14px;}
  .margin {margin-bottom: 35px;}
  .bg-1 { 
    background-color: skyblue; 
    color: black;
    font-family: verdana;
  }
  .bg-2 { 
    background-color: #474e5d; 
    color: #ffffff;
    font-family: verdana;
  }
  .bg-3 { 
    background-color: #7390E1; 
    color: black;
    font-family: verdana;
  }
  .bg-4 { 
    background-color: sandybrown; 
    color: black;
    font-family: verdana;
    align-items: center;
  }
  .container-fluid {
    padding-top: 50px;
    padding-bottom: 50px;
    margin-right: auto;
    margin-left: auto;
    font-family: verdana;
  }
  .table{
      background-color: white;
      border-collapse: collapse;
  }
  
  </style>
</head>

<body>

<div class="site-index">
<!-- Primer contenedor -->
<div class="container-fluid bg-1 text-center">
    <h1 class="margin"><b>Handball-Luc-Sport</b></h1>
    <img src="https://gitlab.com/luciafernandezdam/tienda/-/raw/master/logo/Logo.png" class="img-responsive img-circle margin" style="display:inline" alt="logo" width="250" height="250">
  <h3>Tienda de deportes especializado en Balonmano</h3>
</div>

<!-- Segundo contenedor -->
<div class="container-fluid bg-2 text-center">
    <h3 class="lead">¡Bienvenid@ <?php echo '<b>'. Yii::$app->user->identity->username .'</b>'; ?> a nuestra aplicacion para la administracion de la tienda!</h3>
  <p>Ahora como administrador tiene a su disposicion todas las plantillas de datos. Puede consultar, actualizar y eliminar los datos en cualquier momento.</p>
</div>

<!-- Tercer contenedor (Grid) -->
<div class="container-fluid bg-3 text-center">    
    <h3 style="text-align: center; text-decoration: underline" ><b>Acceso directo a todas las plantillas</b></h3>
  <br>
  <div class="row" style="font-family: verdana">
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <img src="https://static.vecteezy.com/system/resources/thumbnails/000/593/712/small/40_281.jpg" class="img-circle" alt="">
              <div class="caption">
                  <h3><b>Productos</b></h3>
                <p>Consulta de todos los productos que han sido recibidos.</p>
                <br>
                <p><?= Html::a('Acceder a Productos &raquo;', ['productos/index'], ['class' => 'btn btn-default']) ?></p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <img src="https://static.vecteezy.com/system/resources/thumbnails/000/512/692/small/55_Members.jpg" class="img-circle" alt="">
              <div class="caption">
                  <h3><b>Empleados</b></h3>
                <p>Consulta toda la informacion de los empleados de nuestra tienda.</p>
                <br>
                <p><?= Html::a('Acceder a Empleados &raquo;', ['empleados/index'], ['class' => 'btn btn-default']) ?></p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <img src="https://static.vecteezy.com/system/resources/thumbnails/000/550/731/small/user_icon_004.jpg" class="img-circle" alt="">
              <div class="caption">
                  <h3><b>Clientes</b></h3>
                <p>Consulta todos los datos de nuestros clientes que hayan comprado en la tienda.</p>
                <br>
                <p><?= Html::a('Acceder a Clientes &raquo;', ['clientes/index'], ['class' => 'btn btn-default']) ?></p>
              </div>
            </div>
          </div>
            <br>
        </div>
        
        <div class="row">
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <img src="https://static.vecteezy.com/system/resources/thumbnails/000/567/049/small/vector60-2413-01.jpg" class="img-circle" alt="">
              <div class="caption">
                  <h3><b>Proveedores</b></h3>
                <p>Consulta todos los datos de los proveedores que hayan suministrado.</p>
                <br>
                <p><?= Html::a('Acceder a Proveedores &raquo;', ['proveedores/index'], ['class' => 'btn btn-default']) ?></p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
              <img src="https://static.vecteezy.com/system/resources/thumbnails/000/548/263/small/189_-_1_-_Clipboard___List.jpg" class="img-circle" alt="">
              <div class="caption">
                  <h3><b>Categorias</b></h3>
                <p>Consulta todas las categorias de todos los productos de la tienda.</p>
                <br>
                <p><?= Html::a('Acceder a Categorias &raquo;', ['categorias/index'], ['class' => 'btn btn-default']) ?></p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
              <img src="https://static.vecteezy.com/system/resources/thumbnails/000/548/349/small/264_-_1_-_Discount_Label.jpg" class="img-circle" alt="512x512">
              <div class="caption">
                  <h3><b>Ventas</b></h3>
                <p>Consulta todas las ventas que se hayan realizado en la tienda.</p>
                <br>
                <p><?= Html::a('Acceder a Ventas &raquo;', ['ventas/index'], ['class' => 'btn btn-default']) ?></p>
              </div>
            </div>
          </div>
            <br>
        </div>
        
        <div class="row">
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <img src="https://www.iso.org/obp/graphics/grs/3e736a18-9058-409e-9ba2-8b8b880ba4ab_200.png" class="img-circle" alt="">
              <div class="caption">
                  <h3><b>Detalles</b></h3>
                <p>Consulta todos los detalles de nuestros productos que esten a la venta.</p>
                <br>
                <p><?= Html::a('Acceder a Detalles &raquo;', ['detalles/index'], ['class' => 'btn btn-default']) ?></p>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
              <img src="https://static.vecteezy.com/system/resources/thumbnails/000/420/369/small/Road_Signs_2__2882_29.jpg" class="img-circle" alt="">
              <div class="caption">
                  <h3><b>Telefonos</b></h3>
                <p>Consulta todos los telefonos de nuestros clientes de la tienda.</p>
                <br>
                <p><?= Html::a('Acceder a Telefonos &raquo;', ['telefonos/index'], ['class' => 'btn btn-default']) ?></p>
              </div>
            </div>
          </div>
            <br>
        </div>

    </div>

 <!-- Cuarto contenedor (Tabla) -->   
<div class="container-fluid bg-4 text-center">
        <h3 style="text-align: center; font-style: bold" >
            <svg class="bi bi-award" width="2em" height="1em" viewBox="0 0 20 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M9.669.864L8 0 6.331.864l-1.858.282-.842 1.68-1.337 1.32L2.6 6l-.306 1.854 1.337 1.32.842 1.68 1.858.282L8 12l1.669-.864 1.858-.282.842-1.68 1.337-1.32L13.4 6l.306-1.854-1.337-1.32-.842-1.68L9.669.864zm1.196 1.193l-1.51-.229L8 1.126l-1.355.702-1.51.229-.684 1.365-1.086 1.072L3.614 6l-.25 1.506 1.087 1.072.684 1.365 1.51.229L8 10.874l1.356-.702 1.509-.229.684-1.365 1.086-1.072L12.387 6l.248-1.506-1.086-1.072-.684-1.365z"/>
            <path d="M4 11.794V16l4-1 4 1v-4.206l-2.018.306L8 13.126 6.018 12.1 4 11.794z"/>
            </svg>
            <b>Top 10 de los mejores empleados en ventas</b></h3><br>
        <center>
        <table class="table table-striped" border="4" style="text-align: center; width: 85%; background-color: white" >
		<tr>
                    <td><b>Id</b></td>
                    <td><b>Nombre</b></td>
                    <td><b>Apellidos</b></td>
                    <td><b>Edad</b></td>
                    <td><b>Numero Ventas</b></td>
		</tr>

		<?php 
		$sql="SELECT id,nombre,apellidos,edad,num_ventas from empleados ORDER BY num_ventas DESC LIMIT 10";
		$result=mysqli_query($conexion,$sql);

		while($mostrar=mysqli_fetch_array($result)){
		 ?>

		<tr>
			<td><?php echo $mostrar['id'] ?></td>
			<td><?php echo $mostrar['nombre'] ?></td>
			<td><?php echo $mostrar['apellidos'] ?></td>
			<td><?php echo $mostrar['edad'] ?></td>
                        <td><?php echo $mostrar['num_ventas'] ?></td>
		</tr>
	<?php 
	}
	 ?>
	</table>
        </center>
    </div>
</div>
</body>
</html>
<?php } ?>

