<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Iniciar Sesion';
$this->params['breadcrumbs'][] = $this->title;
?>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
    .bg-1 { 
    background-color: skyblue; 
    color: black;
    font-family: verdana;
  }
  .container-fluid {
    padding-top: 50px;
    padding-bottom: 50px;
    margin-right: 80px;
    margin-left: 80px;
    font-family: verdana;
  }
  .text-input{
      text-align: center;
  }
  </style>
</head>
<body style="background-color:#000B31; max-widht:1200px; margin: auto; padding: 0px ">
<div class="site-login">
    
    <div class="container-fluid bg-1 text-center">
    
    <img src="https://gitlab.com/luciafernandezdam/tienda/-/raw/master/logo/Logo.png" class="img-responsive img-circle margin" style="display:inline" alt="logo" width="250" height="200">    
        
    <h1><b><?= Html::encode($this->title) ?></b></h1>

    <p>Complete los siguientes campos para poder acceder a nuestro contenido:</p>
    <br>
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "\n{label}\n<div class=\"col-lg-offset-1 col-lg-4\" align=\"center\">{input}</div>\n<div class=\"col-lg-3\" align=\"center\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-offset-3 col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-lg-offset-2 col-lg-4\">{input} {label}</div>\n<div class=\"col-lg-offset-5 col-lg-8\">{error}</div>",
        ]) ?>

        <div class="form-group">
            <div class="col-lg-offset-3 col-lg-11">
                <?= Html::submitButton('Iniciar sesion', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
    
    </div>
</body>
</div>
