<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<body>
<div class="productos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
           

            'id',
            'nombre',
            'descripcion',
            'precio',
            'stock',
            //'id_proveedor',
            [
                'attribute'=>'Proveedor',
                'value'=>'proveedor.nombre'
            ],
            //'id_categoria',
            [
                'attribute'=>'Categoria',
                'value'=>'categoria.nombre'
            ],
            //'id_empleado',
            [
                'attribute'=>'Empleado',
                'value'=>'empleado.nombre'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
    <p style="text-align: right">
        <?= Html::a('Crear producto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <br>


</div>
</body>
